﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public GameObject gameOverText;
    public GameObject restartButton;
    public GameObject mainMenuButton;
    public Text scoreText;
    public bool gameOver = false;
    public float scrollSpeed = -1.5f;

    public int HighScore { get; protected set; }

    private int score = 0;
    public int Score
    {
        get
        {
            return score;
        }

        protected set
        {           
                score = value;
                if (score > HighScore) {
                    HighScore = score;
                    PlayerPrefs.SetInt("HighScore", HighScore);
                }         
        }
    }

	// Use this for initialization
	void Awake ()
    {
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy (gameObject);
        }

        HighScore = PlayerPrefs.GetInt("HighScore", 0);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void BirdScored()
    {
        if (gameOver)
        {
            return;
        }
        Score++;
        scoreText.text = "Score: " + Score.ToString();
    }

    public void BirdDied()
    {
        gameOverText.SetActive (true);
        mainMenuButton.SetActive(true);
        restartButton.SetActive(true);
        gameOver = true;
    }
}
