﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreUI : MonoBehaviour {

    Text HighScoreText;
    int ScoredSaved;

	void Start () {
        HighScoreText = GetComponent<Text>();
        ScoredSaved = PlayerPrefs.GetInt("HighScore", 0);
    }
	
	void Update () {
        HighScoreText.text = "HighScore: " + (GameControl.instance == null ? ScoredSaved : GameControl.instance.HighScore);
	}
}
